local playerClass, englishClass, classIndex  = UnitClass("player");

-- class index is to not have localization issues. For others, it can be found here: https://wowwiki-archive.fandom.com/wiki/API_UnitClass
if classIndex == 7 then

    lavaburst = GetSpellInfo(51505)
    flameshock = GetSpellInfo(8050)
    totemicRecall = GetSpellInfo(36936)
    callElements = GetSpellInfo(66842)
    lightningBolt = GetSpellInfo(45284)
    chainLightning = GetSpellInfo(421)
    thunderStorm = GetSpellInfo(51490)
    healingWave = GetSpellInfo(25396)
    waterShield = GetSpellInfo(52127)
    fireNova = GetSpellInfo(1535)
    manaSpring = GetSpellInfo(5677)
    wrathOfAirTotem = GetSpellInfo(3738)
    totemOfWrath = GetSpellInfo(30706)

    C_Timer.NewTicker(0.1, function()
        if not mounted and GMR.IsExecuting() and GMR.InLoS("target") and UnitExists("target") then

            -- Healing wave
            if GMR.GetHealth("player") <= 50 then
                if GetSpellInfo(healingWave) and IsUsableSpell(healingWave) then
                    GMR.Cast(healingWave)
                    -- print("healingWave!")
                end
            end
            -- water shield
            if not AuraUtil.FindAuraByName(waterShield, 'player', 'HELPFUL') then
                GMR.Cast(waterShield)
            end
            -- lightning bolt as starter
            if GetSpellInfo(lightningBolt) and IsUsableSpell(lightningBolt) and GMR.IsCastable(lightningBolt, "target") and
                not GMR.IsSpellImmune(lightningBolt, "target") and not GMR.InCombat("player") then
                GMR.Cast(lightningBolt)
            end

            -- thunderStorm if close
            if GetSpellInfo(thunderStorm) and GMR.IsCastable(thunderStorm) and
                GMR.GetDistance("player", "target", "<", 10) then
                GMR.Cast(thunderStorm)
            end
            -- firenova 
            if GetSpellInfo(fireNova) and GMR.GetNumAttackingEnemies("player", 10) >= 3 and
                GMR.IsCastable(fireNova) then
                GMR.Cast(fireNova)
            end

            -- Chain lightning
            if GetSpellInfo(chainLightning) and GMR.GetNumAttackingEnemies("player", 15) >= 2 and
                GMR.IsCastable(chainLightning, 'target') then
                GMR.Cast(chainLightning)
            end

            -- flameshock
            if GetSpellInfo(flameshock) and not GMR.HasDebuff("target", flameshock) and IsUsableSpell(flameshock) and
                GMR.IsCastable(flameshock, "target") and not GMR.IsSpellImmune(flameshock, "target") then
                GMR.Cast(flameshock)
            end
            -- lavaburst when flameshock
            if GetSpellInfo(lavaburst) and IsUsableSpell(lavaburst) and GMR.IsCastable(lavaburst, "target") and
                GMR.InCombat("player") and not GMR.IsSpellImmune(lavaburst, "target") and
                GMR.HasDebuff("target", flameshock) then
                GMR.Cast(lavaburst)
            end

            -- Chain lightning
            if GetSpellInfo(chainLightning) and GMR.GetNumAttackingEnemies("player", 36) >= 3 and
                GMR.IsCastable(chainLightning, 'target') then
                GMR.Cast(chainLightning)
            end
            -- lightning bolt
            if GetSpellInfo(lightningBolt) and IsUsableSpell(lightningBolt) and GMR.IsCastable(lightningBolt, "target") and
                not GMR.IsSpellImmune(lightningBolt, "target") and GMR.InCombat("player") then
                GMR.Cast(lightningBolt)
            end
            -- Totems
            if AuraUtil.FindAuraByName(wrathOfAirTotem, "player", "HELPFUL") or
                AuraUtil.FindAuraByName(manaSpring, "player", "HELPFUL") or AuraUtil.FindAuraByName(totemOfWrath, "player", "HELPFUL") then
                if not UnitAffectingCombat("player") and GetSpellInfo(totemicRecall) and IsUsableSpell(totemicRecall) then
                    GMR.Cast(totemicRecall)
                end
            elseif UnitAffectingCombat("player") and GetSpellInfo(callElements) and IsUsableSpell(callElements) then
                GMR.Cast(callElements)
                -- print("Calloftheelements")
            end
        end
    end)
end
